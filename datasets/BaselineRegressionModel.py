#Building a regression model
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

automobile_df = pd.read_csv('cars_processed.csv')
print(automobile_df.head())
print(automobile_df.shape)

#zaleznosc wieku od miles per gallon
X = automobile_df[['Age']]
Y = automobile_df['MPG']
plt.scatter(automobile_df['Age'], automobile_df['MPG'])
plt.xlabel('Age')
plt.ylabel('MPG')
plt.show()

#rozdzielenie datasetu na czesc testowa i treningowa
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test =train_test_split(X,Y, test_size=0.2)

#REGRESJA LINIOWA
from sklearn.linear_model import LinearRegression
linear_model = LinearRegression(normalize=True).fit(x_train,y_train)
print('Training score:', linear_model.score(x_train,y_train))

#Na podstwie testowych wartosci Age przewiduje MPG
y_pred = linear_model.predict(x_test)

#Sprawdzam czy przewidziane wartosci MPG pokrywaja się z testowymi MPG liczac R score
from sklearn.metrics import r2_score
print('Testing score: ', r2_score(y_test, y_pred))

#Wyswietlenie krzywej regresji na danych testowych
plt.scatter(x_test, y_test)
plt.plot(x_test, y_pred, color='r')

plt.xlabel('Age')
plt.ylabel('Mpg')
plt.show()

#PREDICTION

#biorę wszystkie dane z tabeli oprócz kolumny MPG i chce przepowiedziec MPG
X=automobile_df.drop('MPG', axis=1)
Y=automobile_df['MPG']

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)
linear_model = LinearRegression(normalize=True).fit(x_train, y_train)
print('Training score: ', linear_model.score(x_train, y_train))

y_pred = linear_model.predict(x_test)
print('Testing score: ', r2_score(y_test, y_pred))
