import numpy as np
import pandas as pd
import sklearn
import datetime

#wczytanie datasetu
automobile_df=pd.read_csv('cars.csv')

#probka 10 pierwszych wierszy
print(automobile_df.sample(10))

#zastapienie wszystkich wartosci '?' wartoscia NaN
automobile_df=automobile_df.replace('?',np.nan)

#suma ile takich watosci jest w kazdej kolumnie
print(automobile_df.isna().sum())

#zamieniamy brakujace wartosci MPG wartoscia srednia
automobile_df['MPG']=automobile_df['MPG'].fillna(automobile_df['MPG'].mean())
print(automobile_df.isna().sum())

#usuwam wszystkie wiersze gdzie jest wartosc NaN
automobile_df=automobile_df.dropna()

#sprawdzam jakie typy danych sa w poszczegolnych kolumnach
print(automobile_df.dtypes)

#usuwam niepotrzebne kolumny
automobile_df.drop(['Model', 'stroke', 'bore', 'compression-ratio'], inplace=True, axis=1)

#sprawdzam jakie typy danych sa w poszczegolnych kolumnach
print(automobile_df.dtypes)

#ujednolicenie wartosci roku
print(automobile_df['Year'].str.isnumeric().value_counts())
print(automobile_df['Year'].loc[automobile_df['Year'].str.isnumeric()==False])
extract=automobile_df['Year'].str.extract(r'(^\d{4})', expand=False)
automobile_df['Year']=pd.to_numeric(extract)
print(automobile_df['Year'].dtype)

#add an age column
automobile_df['Age']=datetime.datetime.now().year - automobile_df['Year']
automobile_df.drop(['Year'], axis=1, inplace=True)

print(automobile_df.head())

#sprawdzam kolumne Origin
#print(automobile_df['Origin'].unique())

#ujednolicenie wartosci w kolumnie Origin
automobile_df['Origin']=np.where(automobile_df['Origin'].str.contains('US'), 'US', automobile_df['Origin'])
automobile_df['Origin']=np.where(automobile_df['Origin'].str.contains('Japan'), 'Japan', automobile_df['Origin'])
automobile_df['Origin']=np.where(automobile_df['Origin'].str.contains('Europe'), 'Europe', automobile_df['Origin'])
#sprawdzam kolumne Origin
print(automobile_df['Origin'].unique())

#zamiana wszystkich wartosci w kolumnch Acceleration, Weight oraz Displacement na int64
automobile_df['Acceleration']=pd.to_numeric(automobile_df['Acceleration'], errors='coerce')
automobile_df['Weight']=pd.to_numeric(automobile_df['Weight'], errors='coerce')
automobile_df['Displacement']=pd.to_numeric(automobile_df['Displacement'], errors='coerce')
print(automobile_df.dtypes)


#ujednolicenie wartosci Cylinders
print(automobile_df['Cylinders'].str.isnumeric().value_counts())
print(automobile_df['Cylinders'].loc[automobile_df['Cylinders'].str.isnumeric()==False])
cylinders=automobile_df['Cylinders'].loc[automobile_df['Cylinders'] !='-']
cmean=cylinders.astype(int).mean()
automobile_df['Cylinders']=automobile_df['Cylinders'].replace('-', cmean).astype(int)
print(automobile_df.dtypes)