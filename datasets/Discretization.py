import pandas as pd
import numpy as np

#Generujemy tablicę
X=np.array([-7, 2, -3, -11, 14, 6, 8])

#Wyznaczamy 4 kategorie
categories = pd.cut(X,4)

#print(categories)
#print(categories.codes)

#Pokazuje bins, które liczby sa na krańcach kategorii
#print(pd.cut(X,4,retbins=True))

marks = np.array([70,20,30,99,40,16,80])
categories,bins = pd.cut(marks, 4, retbins=True, labels = ['poor', 'avarage', 'good', 'excellent'])
print(categories)

from sklearn.preprocessing import KBinsDiscretizer
#This time data will be represented as 2D list
marks = [[70],[20],[30],[99],[40],[16],[80]]

enc = KBinsDiscretizer(n_bins=4, encode='ordinal', strategy='uniform')
print(enc.fit(marks))
print(enc.transform(marks))
print(enc.bin_edges_)

#This time it will be 3D data
X= [[-21, 41, -14],
    [-13, 23, -31],
    [9,30,-5],
    [0,24,-27]]
enc = KBinsDiscretizer(n_bins=4, encode='ordinal', strategy='quantile')
print(enc.fit(X))
X_trans = enc.transform(X)
print(X_trans)

print(enc.bin_edges_) #Widzimy że dla każdej kolumny inaczej zdefiniowane są biny (w kazdej kolumnie sa inne wartosci, to dlatego)

#We try to recreate original points
print(enc.inverse_transform(X_trans)) #Punkty zostały dobrane (ozyskane) tak, że wzięto wartości edges bina i wyznaczono środek

# ----------
# Teraz budujemy model regresji
automobile_df = pd.read_csv('auto_mpg_processed.csv')

#Split into X variables and Y labels
X = automobile_df[['Horsepower']]
Y = automobile_df['MPG']

#import matplotlib.pyplot as plt
#plt.figure(figsize=(10,7))
#plt.plot(X,Y, 'o', c='y')
#plt.show()

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=0.2)

reg = LinearRegression().fit(x_train,y_train)
y_pred = reg.predict(x_test)
print(y_pred)

from sklearn.metrics import r2_score
print("r2 score: ", r2_score(y_test, y_pred)) #Dopasowanie modelu


#Now we use KBinsDescretiezer to separate bins

enc = KBinsDiscretizer(n_bins=20, encode='ordinal')
x_binned = enc.fit_transform(x_train)
print(x_binned[:10])
x_test_binned = enc.transform(x_test)
reg = LinearRegression().fit(x_binned,y_train)
y_pred = reg.predict(x_test_binned)
print(y_pred)
print("r2 score: ", r2_score(y_test, y_pred)) #Dopasowanie modelu
#Descritizing our Horsepower valus has improve our model
