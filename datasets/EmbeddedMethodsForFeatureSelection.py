# Embedded methods for feature selection - LASSO REGRESSION MODEL
import numpy as np
import pandas as pd

automobile = pd.read_csv('cars_processed.csv')
print(automobile.head())

X = automobile.drop(['MPG', 'Origin'], axis=1)
Y = automobile['MPG']

from sklearn.linear_model import Lasso

lasso = Lasso(alpha=0.8)
lasso.fit(X, Y)

predictors = X.columns # Here, predictors are all column
coef = pd.Series(lasso.coef_, predictors).sort_values() # It calculates coefficient value for each predictor
print(coef) # The most important predictor are age and weight of a car

# Because of that we select these features
lasso_features = ['Age', 'Weight']
X[lasso_features].head()

# Let's calculate which features are important using DecisionTreeRegressor
from sklearn.tree import DecisionTreeRegressor

decision_tree = DecisionTreeRegressor(max_depth=4)
decision_tree.fit(X, Y)

predictors = X.columns
coef = pd.Series(decision_tree.feature_importances_, predictors).sort_values()
print(coef)

decision_tree_features = ['Displacement', 'Horsepower']
X[decision_tree_features].head()

#We build a model using Linear Regression
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

def build_model(X, Y, test_frac):
    x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=test_frac)
    model=LinearRegression().fit(x_train, y_train)
    y_pred = model.predict(x_test)

    print("Test score: ", r2_score(y_test, y_pred))

build_model(X[lasso_features], Y, 0.2)
build_model(X[decision_tree_features], Y, 0.2)



