import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#We will use diabetes to build classification model to predict wheter the have diabetes or not
diabetes = pd.read_csv('diabetes.csv')

#First tool which helps us visualize correlation is corr coeff
diabetes_corr = diabetes.corr()
print(diabetes_corr)

#Very useful tool to visualize correlation is heatmap
import seaborn as sns

#plt.figure(figsize=(12,8))
#sns.heatmap(diabetes_corr, annot=True)
#plt.show()

#ANother tool - yellowbrick package
#I want to see if these features (X) are possitively or negatively correlated with Age
X=diabetes[['Insulin', 'BMI', 'BloodPressure', 'DiabetesPedigreeFunction']]
Y=diabetes['Age']

feature_names = X.columns
print(feature_names)

from yellowbrick.target import FeatureCorrelation

visualizer = FeatureCorrelation(labels=feature_names, method='pearson')
visualizer.fit(X,Y)
#visualizer.poof() #Insulin is negatively correlated but the rest is positively correlated

#You can also visualize this as a dataframe
score_df = pd.DataFrame({'Feature_names' : visualizer.features_, 'Scores' : visualizer.scores_})
print(score_df)

#Now we want to see correlation of outcome, wheter a person has diabetes or not
X=diabetes.drop('Outcome', axis =1)
Y=diabetes['Outcome']
feature_names=X.columns
visualizer = FeatureCorrelation(labels=feature_names, method='pearson')
visualizer.fit(X,Y)
#visualizer.poof()

#Pregnancies are discrete numeric values
#I want to implement boolean vector to determine wheter a feature is discrete value or not

discrete_features = [False for _ in range(len(feature_names))]
discrete_features[0] = True

visualizer = FeatureCorrelation(labels=feature_names, method='mutual_info-classification')
visualizer.fit(X,Y, discrete_features=discrete_features, random_state = 0)
visualizer.poof()

