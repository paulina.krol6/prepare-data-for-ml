import numpy as np
import pandas as pd

diabetes = pd.read_csv('diabetes_processed.csv')
#print(diabetes.head(10))

#Dzielimy dane na feature i label
diabetes_features = diabetes.drop('Outcome', axis=1);
diabetes_label = diabetes [['Outcome']]

#Randomly indtroduce missing values, this mask has the same shape as diabetes_features,
mask = np.random.randint(0,100, size=diabetes_features.shape).astype(bool)
# logical not -> all zero values will be true, non-zero values as false
mask = np.logical_not(mask)

#All values where the mask is true (=0), I am going to set NaN
diabetes_features[mask]= np.nan

#Create the pipeline and apply transformations
from sklearn.pipeline import make_pipeline
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
#To split data, decision tree classifier
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

#80% data used to training, 20% to test
x_train, x_test, y_train, y_test = train_test_split(diabetes_features, diabetes_label, test_size=0.2)

# Transformation, ue Simple Imputer to fill missing values, in this case to mean to all existing features
transformer = ColumnTransformer(
     transformers=[('features', SimpleImputer(strategy='mean'), [0, 1, 2, 3, 4, 5, 6, 7])]
)

#Create pipeline with decision tree classifier
clf = make_pipeline(transformer, DecisionTreeClassifier(max_depth=4))
clf = clf.fit(x_train,y_train)
print(clf.score(x_train, y_train))

y_pred = clf.predict(x_test)

from sklearn.metrics import accuracy_score
print(accuracy_score(y_pred,y_test))