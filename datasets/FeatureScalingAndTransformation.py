import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math

#Ustawić precyzję, trzy miejsca po przecinku
np.set_printoptions(precision=3)

diabetes = pd.read_csv('diabetes_processed.csv')

features_df = diabetes.drop('Outcome', axis = 1)
target_df = diabetes['Outcome']

#Chcemy zobaczyć jakie są wartości min, max naszych wartosci, okazuje się ze są bardzo zróżnicowane
#Model nie działa dobrze jesli przedziały są aż tak zróżnicowane
print(features_df.describe())

from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler(feature_range=(0,1))
rescaled_features = scaler.fit_transform(features_df)

print(rescaled_features.shape)

#This is numpy array so I must view it like this
print(rescaled_features[0:5])

#Musimy zamienić to na dataframe przymując nazwy kolumn takie jak features_df
rescaled_features_df = pd.DataFrame(rescaled_features, columns=features_df.columns)
print(rescaled_features_df.describe()) # we see that each feature was rescaled from 0 to 1

#Kolejną techniką jest standaryzacja
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
scaler = scaler.fit(features_df)
standarized_features = scaler.transform(features_df)

print(standarized_features[0:5]) # Positive values - above the mean, negative values - below the mean

standarized_features_df = pd.DataFrame(standarized_features, columns=features_df.columns)
print(standarized_features_df.describe())

standarized_features_df.boxplot(figsize=(10,7), rot=45)
#plt.show()

#Normalization, converts features to have normalized magnitude, but there are different kinds of magnitude (this time - 11)
from sklearn.preprocessing import Normalizer
normalizer = Normalizer(norm='l1')
normalized_features = normalizer.fit_transform(features_df)
l1_normalized_featuers_df = pd.DataFrame(normalized_features, columns=features_df.columns)
print(l1_normalized_featuers_df.iloc[0])
print(l1_normalized_featuers_df.iloc[0].abs().sum())

#Sum of the values for all features will be 1.0
#If you use l2, the sum of sqaures (pow(2)) will be equal to 1.0

#Other normalizing kind is 'max'
normalizer = Normalizer(norm='max')
normalized_features = normalizer.fit_transform(features_df)
max_normalized_features_df = pd.DataFrame(normalized_features, columns=features_df.columns)
#The maximum value will be set to 1.0, and the rest will be some fraction of this original maximum value


#Binarizer uses the threshold to convert values to 0 - 1 values
from sklearn.preprocessing import Binarizer
binarizer = Binarizer(threshold=float((features_df[['Pregnancies']]).mean()))
binarized_features = binarizer.fit_transform(features_df[['Pregnancies']])
#This time we want to specify the number of pregnancies below and above the mea

print(binarized_features[0:10])


#Now I want to use binarization for all columns

for i in range(1, features_df.shape[1]):
    scaler = Binarizer(threshold=float((features_df[[features_df.columns[i]]]).mean())).\
                                        fit(features_df[[features_df.columns[i]]])
    new_binarized_feature = scaler.transform(features_df[[features_df.columns[i]]])
    binarized_features = np.concatenate((binarized_features, new_binarized_feature), axis=1)


print(binarized_features[0:10])


#BUILDING A MODEL
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

def build_model(X,Y,test_frac):
    x_train,x_test,y_train,y_test = train_test_split(X,Y,test_size=test_frac)
    model = LogisticRegression(solver='liblinear').fit(x_train,y_train)
    y_pred = model.predict(x_test)
    print("test score: ", accuracy_score(y_test,y_pred))

#Sprawdzamy accuracy score modelu używając różnych danych
build_model(rescaled_features, target_df, 0.2)
build_model(standarized_features, target_df, 0.2)
build_model(normalized_features, target_df, 0.2)
build_model(binarized_features, target_df, 0.2)