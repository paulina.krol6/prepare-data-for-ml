#Performing feature selection using a missing values threshold and a variance threshold
import pandas as pd
import numpy as np

diabetes_data =pd.read_csv('diabetes.csv')

#Zamieniamy każde 0 na missing value (Ale nie w pregnancies no bo to jest mozliwe)
diabetes_data['Glucose'].replace(0,np.nan,inplace=True)
diabetes_data['BloodPressure'].replace(0,np.nan,inplace=True)
diabetes_data['SkinThickness'].replace(0,np.nan,inplace=True)
diabetes_data['Insulin'].replace(0,np.nan,inplace=True)
diabetes_data['BMI'].replace(0,np.nan,inplace=True)

#Sprawdzamy dokładną ilosć missing values oraz jaki to stanowi procent danych
print(diabetes_data.isnull().sum())
print((diabetes_data['Glucose'].isnull().sum()/len(diabetes_data))*100) #Dla glucose <1% danych jest missing
print((diabetes_data['BloodPressure'].isnull().sum()/len(diabetes_data))*100) #Tutaj juz 4,55 %
print((diabetes_data['SkinThickness'].isnull().sum()/len(diabetes_data))*100) #29,5 %
print((diabetes_data['Insulin'].isnull().sum()/len(diabetes_data))*100) #48,69%
print((diabetes_data['BMI'].isnull().sum()/len(diabetes_data))*100) #1,43 %

print(diabetes_data.columns)
#Wywalamy te kolumny gdzie występują missing values przekraczające konkretny threshold. za pomoca dropna
diabetes_data_trimmed = diabetes_data.dropna(thresh=int(diabetes_data.shape[0]*0.9), axis=1)
print(diabetes_data_trimmed.columns) # some columns has been dropped

#-----------
#Building classification model
diabetes_data=pd.read_csv('diabetes_processed.csv')
X=diabetes_data.drop('Outcome', axis =1)
Y=diabetes_data['Outcome']

print(X.var(axis=0)) #Calculate variance
#A feature whose value does not very much, does not contain very much predicive information
#But for now, scales are very different so we have to scale them and then compare

from sklearn.preprocessing import minmax_scale
X_scaled = pd.DataFrame(minmax_scale(X,feature_range=(0,10)), columns=X.columns) #scale all features to be in range 0 to 10
print(X_scaled.var())

#Now if we know the variance, we want to select features with predictive power
from sklearn.feature_selection import VarianceThreshold
select_features = VarianceThreshold(threshold=1.0)
X_new = select_features.fit_transform(X_scaled)
print(X_new.shape) #One feature was dropped, it was skin thickness because it has the smallest variance value

var_df=pd.DataFrame({'feature_names': list(X_scaled), 'variances' : select_features.variances_})
print(var_df)

X_new = pd.DataFrame(X_new)
print(X_new.head) #Unfortunately this dataframe does not have column names

selected_features=[]
for i in range(len(X_new.columns)):
    for j in range(len(X_scaled.columns)):
        if(X_new.iloc[:,i].equals(X_scaled.iloc[:,j])):
            selected_features.append(X_scaled.columns[j])
print(selected_features)
