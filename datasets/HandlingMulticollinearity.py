import pandas as pd

automobile_df = pd.read_csv('cars_processed.csv')
print(automobile_df.describe())

from sklearn import preprocessing
#Let's standarize all features before we apply our model by using "scale" function
#We want to build regression model
automobile_df[['Cylinders']] = preprocessing.scale(automobile_df[['Cylinders']].astype('float64'))
automobile_df[['Displacement']] = preprocessing.scale(automobile_df[['Displacement']].astype('float64'))
automobile_df[['Horsepower']] = preprocessing.scale(automobile_df[['Horsepower']].astype('float64'))
automobile_df[['Weight']] = preprocessing.scale(automobile_df[['Weight']].astype('float64'))
automobile_df[['Acceleration']] = preprocessing.scale(automobile_df[['Acceleration']].astype('float64'))
automobile_df[['Age']] = preprocessing.scale(automobile_df[['Age']].astype('float64'))
print(automobile_df.describe())

print(automobile_df.shape)

from sklearn.model_selection import train_test_split
X=automobile_df.drop(['MPG', 'Origin'], axis=1) #Delete Origin just for convience
Y=automobile_df['MPG']
x_train, x_test, y_train, y_test = train_test_split(X,Y,test_size=0.2)

from sklearn.linear_model import LinearRegression
linear_model = LinearRegression(normalize=True).fit(x_train,y_train)

print("Training score: ", linear_model.score(x_train,y_train))
y_pred = linear_model.predict(x_test)

#Tutaj mamy model regresji (ciągłe wartości) dlatego nie liczymy accuracy score tyko R2 score
from sklearn.metrics import r2_score
print("R2 Score: ", r2_score(y_test, y_pred)) #Very good

#When you have multiple features, better way to measure how good your model is, is to calculate adjusted_r2

def adjusted_r2(r_square, labels, features):
    adj_r_square = 1 - ((1-r_square) * (len(labels)-1)) / (len(labels) - features.shape[1] -1)
    return adj_r_square

print("Adjusted_r2_score: ", adjusted_r2(r2_score(y_test, y_pred), y_test, x_test))


#When you look at correlations, you can see that Cylinders, Horsepower and Weight all have very high (and similar)
#correlation with the Displacement
#So you do not need to use all of them

features_corr = X.corr()
print(features_corr)

print(abs(features_corr) >0.8) # Using this correlation martix we want to see which features are highly correlated

#In to avoid multicollinearity that exist in our feature I want to drop a few columns
trimmed_features_df = X.drop(['Cylinders', 'Displacement', 'Weight'], axis =1)
trimmed_features_df_corr = trimmed_features_df.corr()
print(trimmed_features_df_corr)

#USING VARIANCE INFLATION FACTOR if we want to throw away features that are not correlated
from statsmodels.stats.outliers_influence import variance_inflation_factor
#It shows correlation for feature "i" and other features other then "i"
vif = pd.DataFrame()
vif["VIF Factor"] = [variance_inflation_factor(X.values,i) for i in range(X.shape[1])]
vif["features"] = X.columns
vif.round(2)
print(vif) # Value of VIF Factor =1 -> not correlated, 1-5: moderately correlated, >5: highly correlated
#We see that Displacement and Weight have the highest score so I am going to drop them

X=X.drop(['Displacement', 'Weight'], axis=1)

#I want to calculate VIF again
vif = pd.DataFrame()
vif["VIF Factor"] = [variance_inflation_factor(X.values,i) for i in range(X.shape[1])]
vif["features"] = X.columns
vif.round(2)
print(vif) # Now all factors are below 5, so these are the features I am going to use in my REGRESSION ANAYSIS

#Potrzebuję znależć te zmienne independent zeby z nich móc przewidywać inne
#Wywalam te z wysokim VIF i robię od nowa model

X=automobile_df.drop(['MPG', 'Origin', 'Displacement', 'Weight'], axis=1) #Delete Origin just for convience
Y=automobile_df['MPG']
x_train, x_test, y_train, y_test = train_test_split(X,Y,test_size=0.2)

linear_model = LinearRegression(normalize=True).fit(x_train,y_train)

print("Training score: ", linear_model.score(x_train,y_train))
y_pred = linear_model.predict(x_test)
print("R2 Score: ", r2_score(y_test, y_pred))
print("Adjusted_r2_score: ", adjusted_r2(r2_score(y_test, y_pred), y_test, x_test))