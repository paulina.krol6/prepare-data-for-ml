import pandas as pd
import numpy as np

from sklearn.impute import MissingIndicator

#missing values are marked as -1
features = [[4, 2, 1],
            [24, 12, 6],
            [8, 4, 2],
            [28, 14, 7],
            [32, 16, -1],
            [600, 300, 150],
            [-1, 60, 30],
            [-1, 4, 1]]

indicator = MissingIndicator(missing_values=-1) #tu przetwarza tylko te kolumny ktore maja braki
mask_missing_values_only = indicator.fit_transform(features)
print(mask_missing_values_only)

#w ilu kolumnach sa te missing values -> [0 2] czyli w 0 i 2 kolumnie, a data mają trzy kolumny
print(indicator.features_)

indicator = MissingIndicator(missing_values=-1, features="all") #tu przetwarza wsyzstkie
mask_all = indicator.fit_transform(features)
print(mask_all)
print(indicator.features_) # pokauzuje 0 1 i 2 kolumne