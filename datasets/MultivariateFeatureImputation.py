import numpy as np
import pandas as pd

#Zastosowanie IterativeImputer
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer

imp = IterativeImputer(max_iter=100, random_state=0)

features = [[4, 2, 1],
            [24, 12, 6],
            [8, np.nan, 2],
            [28, 14, 7],
            [32, 16, np.nan],
            [600, 300, 150],
            [np.nan, 60, 30],
            [np.nan, np.nan, 1]]
#prediction
imp.fit(features)
print(imp.transform(features))

#test
X_test = [[np.nan, 24, 12],
          [36, np.nan, np.nan],
          [100, np.nan, 25],
          [np.nan, 6, 3],
          [np.nan, 8, np.nan]]
#wynik testu
print(imp.transform(X_test))

#Wczytanie tabeli diabates_processed_incomplete
diabetes = pd.read_csv('diabetes_processed_incomplete.csv')

diabetes.shape
diabetes.info()
print(diabetes.isnull().sum())

#Usuwam kolumnę Outcome żeby ją przewidzieć
diabetes_features = diabetes.drop('Outcome', axis=1)
diabetes_label = diabetes[['Outcome']]

#Zastosowanie IterativeImputera
imp = IterativeImputer(max_iter=10000, random_state=0)
imp.fit(diabetes_features)
#utworzenie nowej tabeli, ktora byłaby z wartosciami przewidzianymi
diabetes_features_arr = imp.transform(diabetes_features)
print(diabetes_features_arr.shape)

#ustawienie nazwy kolumn nowej takiej jak ta stara
diabetes_features = pd.DataFrame(diabetes_features_arr, columns=diabetes_features.columns)
#połączenie dwóch obiektów
diabetes = pd.concat([diabetes_features, diabetes_label], axis=1)

diabetes.to_csv('diabetes_processed.csv', index=False)

