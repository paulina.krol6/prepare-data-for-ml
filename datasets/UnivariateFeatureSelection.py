# FILTER METHODS (using chi2 test, ANOVA)

# Univariate feature selection is when we examine each feature individually
import pandas as pd
import numpy as np

diabetes_data = pd.read_csv('diabetes.csv')

# Original X features, and X_new - features that were selected by variuous statistical techniques
def get_selected_features(X, X_new):
    selected_features = []

    for i in range(len(X_new.columns)):
        for j in range(len(X.columns)):
            if X_new.iloc[:, i].equals(X.iloc[:, j]):
                print(X.columns[j])
                selected_features.append(X.columns[j])
    return selected_features

# This time we chose Chi2 and KBest
from sklearn.feature_selection import chi2 #Chi2 is a measure of dependancy between variables
from sklearn.feature_selection import SelectKBest #to select K best features

X = diabetes_data.drop('Outcome', axis =1)
Y = diabetes_data['Outcome']

# convert all data to one format
X = X.astype(np.float64)

test = SelectKBest(score_func=chi2, k=4) #we choose k=4 best features, scoring by chi2 function
fit = test.fit(X, Y)
print(fit.scores_)

feature_score = pd.DataFrame()
for i in range(X.shape[1]):
    new = pd.DataFrame({'Features': X.columns[i], 'Score': fit.scores_[i]}, index=[i])
    feature_score = pd.concat(([feature_score, new]))

print(feature_score)

X_new = fit.transform(X)
X_new = pd.DataFrame(X_new)
print(X_new.head())

selected_features = get_selected_features(X, X_new)
print(selected_features)

chi2_best_features = X[selected_features]

# Another feature selection technique - ANOVA
# F-value as a measure of dependency, to determine most relevant features
from sklearn.feature_selection import f_classif, SelectPercentile
test = SelectPercentile(f_classif, percentile=80) #it will choose feautres in the top 80%
fit = test.fit(X,Y)
print(fit.scores_)

X_new = fit.transform(X)
X_new = pd.DataFrame(X_new)
print(X_new.head()) #This do not have feature name so we have to extract them

selected_features = get_selected_features(X, X_new)
selected_features

#Use this selected featues in new dataframe
f_classif_best_features = X[selected_features]

from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

def build_model(X, Y, test_frac):
    x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=test_frac)
    model=LogisticRegression(solver='liblinear').fit(x_train, y_train)
    y_pred = model.predict(x_test)

    print("Test score: ", accuracy_score(y_test, y_pred))

# Applying this model to the original data
build_model(X, Y, 0.2)
# Applying the model to the four most relevant features
build_model(chi2_best_features, Y, 0.2)
# Using ANOVA selected features
build_model(f_classif_best_features, Y, 0.2)