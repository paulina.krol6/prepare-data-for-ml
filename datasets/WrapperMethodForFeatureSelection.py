# WRAPPER METHODS - based on specific ML algorithm
import pandas as pd
import numpy as np

diabetes_data = pd.read_csv('diabetes.csv')
X=diabetes_data.drop('Outcome', axis =1)
Y=diabetes_data['Outcome']

from sklearn.feature_selection import RFE #Recursive Feature Elimination
from sklearn.linear_model import LogisticRegression

model = LogisticRegression(solver='liblinear')
rfe = RFE(model, n_features_to_select=4)

fit = rfe.fit(X, Y)
print("Num features: ", fit.n_features_)
print("Selected Features: ", fit.support_)
print("Feature ranking: ", fit.ranking_)

# I am going to put this in a single data frame
feature_rank = pd.DataFrame({'columns': X.columns, 'ranking': fit.ranking_, 'selected': fit.support_})
print(feature_rank)

# I am going to extract these selected features
recursive_feature_names = feature_rank.loc[feature_rank['selected'] == True]
print(recursive_feature_names)

# Now extract them into a single dataframe
print(X[recursive_feature_names['columns'].values].head())
recursive_features = X[recursive_feature_names['columns'].values] #all rows, but only this specified columns

# Another technique WRAPPER METHOD - FORWARD ELIMINATION
from mlxtend.feature_selection import SequentialFeatureSelector
from sklearn.ensemble import RandomForestClassifier

feature_selector = SequentialFeatureSelector(RandomForestClassifier(n_estimators=10), #it uses 10 indiviual decision trees
                                             k_features=4,
                                             forward=True, #forward selections
                                             scoring='accuracy', #scoring is accuracy score
                                             cv=4)
features = feature_selector.fit(np.array(X), Y)

#Getting the selected feature names
forward_elimination_feature_names = list(X.columns[list(features.k_feature_idx_)])
print(forward_elimination_feature_names)

forward_elimination_features = X[forward_elimination_feature_names]

# Another WRAPPER METHOD - BACKWARD SELECTION
feature_selector = SequentialFeatureSelector(RandomForestClassifier(n_estimators=10), #it uses 10 indiviual decision trees
                                             k_features=4,
                                             forward=False, #backward selections
                                             scoring='accuracy', #scoring is accuracy score
                                             cv=4)
features = feature_selector.fit(np.array(X), Y)

backward_elimination_feature_names = list(X.columns[list(features.k_feature_idx_)])
print(backward_elimination_feature_names)

backward_elimination_features = X[backward_elimination_feature_names]


# Now we will use classifier model
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

def build_model(X, Y, test_frac):
    x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=test_frac)
    model=LogisticRegression(solver='liblinear').fit(x_train, y_train)
    y_pred = model.predict(x_test)

    print("Test score: ", accuracy_score(y_test, y_pred))

build_model(X, Y, 0.2)
build_model(recursive_features, Y, 0.2)
build_model(forward_elimination_features, Y, 0.2)
build_model(backward_elimination_features, Y, 0.2)


